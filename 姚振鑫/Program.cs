﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp24
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable hashtable = new Hashtable();
            Console.WriteLine(hashtable.Count);//集合中存放的元素
            hashtable.Add(1, "小明");//增加元素
            hashtable.Remove(1);//根据指定的Key值移除对应的集合元素
            hashtable.Clear();//清空所有元素
            hashtable.ContainsKey(1);//判断key值里是否还有1
            hashtable.ContainsValue("小明");//判断Value值里是否含有“小明”


            
            Hashtable name = new Hashtable();
            name.Add(1, "小明");
            name.Add(2, "小红");
            name.Add(3, "小笑");
            Console.WriteLine("请输入id查询是否有小明");
            int id = int.Parse(Console.ReadLine());
            bool flag = name.ContainsKey(id);
            if (flag)
            {
                Console.WriteLine("你查找的名字为{0}", name[id].ToString());
            }
            else 
            {
                Console.WriteLine("你查找的名字没有");
            }
            Console.WriteLine("表中所有的名字");
            foreach (DictionaryEntry v in name) 
            {
                int key = (int)v.Key;
                string value = v.Value.ToString();
                Console.WriteLine("{0}{1}",key,value);
            }
            
        }
    }
}
