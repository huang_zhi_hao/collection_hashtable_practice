﻿using System;
using System.Collections;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            //创建了一个键值对集合对象
            Hashtable ht = new Hashtable();
            //ht.Add(1,"张三");
            //ht.Add(2, "true");
            //ht.Add(3, "男");
            //ht.Add(false,"错误的");
            //ht[4] = "张三";  //这也是一种添加数据的方式，  键不能重复，值可以重复
            //ht[1] = "把张三覆盖掉";  //使用这种方式添加数据如果添加的数据的键重复了，会把数据覆盖掉，替换为要添加的数据
            //ht.Remove(3);    //根据键删除数据
            //if (ht.ContainsKey(1))
            //{
            //    Console.WriteLine("已经包含这个键了");
            //}
            //else
            //{
            //    ht[1] = "新来的";
            //}
            //foreach (var item in ht.Keys)
            //{
            //    Console.WriteLine("键是{0}，值是{1}",item,ht[item]);
            //}
            //在键值对集合中，是根据键去找值得
            //Console.WriteLine(ht[1]);
            //Console.WriteLine(ht[2]);
            //Console.WriteLine(ht[3]);
            //Console.WriteLine(ht[false]);
            //Console.WriteLine("-----------------------------------------");
            //for(int i = 0; i < ht.Count; i++)
            //{
            //    Console.WriteLine(ht[i]);
            //}

            //利用Hashtable 实现简繁转换
            string Jian = "我是电脑";
            string Fan = "我是電腦";
            Console.WriteLine("请输入简体字，为你转换为繁体字");
            string Input = Console.ReadLine();
            for(int i = 0; i < Input.Length; i++)
            {
                ht.Add(Jian[i],Fan[i]);
                if (ht.ContainsKey(Jian[i]))
                {
                    Console.Write(Fan[i]);
                }
                else
                {
                    Console.Write(Jian[i]);
                }
            }
        }
    }
}
