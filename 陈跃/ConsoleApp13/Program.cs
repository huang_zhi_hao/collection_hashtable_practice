﻿using System;
using System.Collections;

namespace ConsoleApp13
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable hashtable = new Hashtable();
            hashtable.Add(1, "臭豆腐");
            hashtable.Add(2, "鸡肉卷");
            hashtable.Add(3, "奶茶");
            Console.WriteLine("请选择你要吃的食物编号");
            int id = int.Parse(Console.ReadLine());
            bool a = hashtable.ContainsKey(id);
            if (a)
            {
                Console.WriteLine("你要吃的是：{0}",hashtable[id].ToString());
            }
            else
            {
                Console.WriteLine("没有你要吃的东西，请重新选择");
            }
            Console.WriteLine("所有食物如下：");
            foreach (DictionaryEntry d in hashtable)
            {
                int key = (int)d.Key;
                string value = d.Value.ToString();
                Console.WriteLine("食物编号：{0}，食物名称：{1}", key, value);
            }
        }
    }
}
