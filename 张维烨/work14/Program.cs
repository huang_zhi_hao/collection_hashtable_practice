﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace work14
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable ht = new Hashtable();
            //添加内容
            ht.Add("北京", "是中国的首都");
            ht.Add("福州", "是福建的省会城市");
            ht.Add("成都", "是四川的省会城市");
            ht.Add("沈阳", "是辽宁的省会城市");
            ht.Add("南京", "是江苏的省会城市");
            ht.Add("杭州", "是浙江的省会城市");
            ht.Add("憨婷", "是SB");
            Console.WriteLine("请输入城市：");
            string cityName = Console.ReadLine();
            bool flag = ht.ContainsKey(cityName);
            if (flag)
            {
                Console.WriteLine("{0}{1}", cityName,ht[cityName].ToString());
            }
            else
            {
                Console.WriteLine("您查找的城市暂时不存在！");
            }
            Console.WriteLine();

            //根据key值移除错误的信息
            ht.Remove("憨婷");//移除"憨婷"key值

            Console.WriteLine("部分的城市信息如下：");
            foreach (DictionaryEntry d in ht)
            {
                object key = d.Key;
                string value = d.Value.ToString();
                Console.WriteLine("城市名：{0}，城市介绍：{1}", key, value);
            }
        }
    }
}
