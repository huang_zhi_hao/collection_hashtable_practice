﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp17
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable aa = new Hashtable();
            aa.Add(1,"春风阁");
            aa.Add(2,"夏禹阁");
            aa.Add(3,"金秋阁");
            aa.Add(4,"冬雪阁");
            aa.Add(5,"幽兰阁");

            Console.WriteLine("所有房间编号及名称为：");
            foreach (DictionaryEntry vs in aa)
            {
                int key = (int)vs.Key;
                string value = vs.Value.ToString();
                Console.WriteLine("房间编号:{0}，房间名称为:{1}",key,value);
            }

            Console.WriteLine();

            Console.WriteLine("请输入包间号：");
            int id = int.Parse(Console.ReadLine());
            var flag = aa.ContainsKey(id);

            if (flag)
            {
                Console.WriteLine("该房间存在且房间名称为:{0}", aa[id].ToString());
            }
            else
            {
                Console.WriteLine("该房间不存在");
                Console.WriteLine("房间号都全部给你看了你还能选错？搞事情？？讨打吗？？？");
            }

        }
    }
}
